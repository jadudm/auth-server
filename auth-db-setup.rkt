#lang racket

(require db)
(require "mql.rkt")
(require "auth.rkt")

(define conn (sqlite3-connect #:database "users.sqlite3"
                              #:mode 'create))

(define macro-table
  (create-table 
   macros
   (uid integer primary-key)
   (name text)
   (macro text)
   ))

(define (create-tables conn)
  (query-exec conn macro-table))

(define c 0)
(define (populate-tables conn)
  (for ([(k v) macros])
    (set! c (add1 c))
    (printf "K: ~a V: ~a~n" k v)
    (define statement
      (insert (table macros)
              (columns name macro)
              (values ,(format "\"~a\"" k)
                      ,(format "\"~a\"" v))))
    (query-exec conn statement)))