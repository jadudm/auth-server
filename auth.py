import types

# Rules will have to be encoded in JSON for inter-language portability.

# Returns empty; should return a boolean.
rule01 = {'rule' : []}
# Returns an error.
rule02 = {'rule' : [{'and' : []}]}
# Evaluates correctly
rule03 = {'rule' : [{'and' : [True, False]}]}
rule04 = {'rule' : [{'and' : [True, True]}]}
rule05 = {'rule' : [{'or'  : [False, True]}]}

def parse (rule):
    # Start with non-iterable outcomes
    if isinstance(rule, bool):
        return rule
    # Now handle cases where you have a dictionary/list
    elif 'rule' in rule:
        # Return False by default.
        results = map(parse, rule['rule'])
        if len(rule['rule']) < 1:
            result = False
        else:
            result = True
        # 'and' all of the results together.
        for r in results:
            result = result and parse(r)
        return result
    # If we don't have anything in the and, it defaults to False.
    elif ('and' in rule):
        if len(rule['and']) < 1:
            result = False
        else:
            result = True
        for r in rule['and']:
            result = result and parse(r)
        return result
    # If we don't have anything in the or, it defaults to False.
    elif ('or' in rule):
        if len(rule['or']) < 1:
            result = False
        else:
            result = True
        result = True
        for r in rule['or']:
            result = result or parse(r)
        return result
    else:
        return "Error in: %s" % rule

rules   = [rule01, rule02, rule03, rule04, rule05]
results = [False,  False,  False,  True,   True]
for r,res in zip(rules, results):
    print ("%s\t=>\t%s" % (parse (r), res))
